
# Dotfiles backup

## Create a new backup
The following instructions describe how to create a new git backup of the dotfiles configuration files located in the HOME directory.

1. Init
```sh
git init --bare $HOME/.git_config_backup
```
2. Add alias in shell file
```sh
alias config='/usr/bin/git --git-dir=$HOME/.git_config_backup/ --work-tree=$HOME'
```
3. Do not show untracked files
```sh
config config --local status.showUntrackedFiles no
```

4. Use config as git
```sh
config remote add origin https://gitlab.com/Louka-Yerly/dotfiles_backup.git
config add file
config commit -m file
config push
```


## Restore a backup on a new computer

1. Avoid self tracking
```sh
echo ".git_config_backup" >> .gitignore
```
2. Clone git repos configs
```sh
git clone --bare <remote-git-repo-url> $HOME/.git_config_backup
```
3. Create the alias (add this in the rc file (.zshrc))
```sh
alias config='/usr/bin/git --git-dir=$HOME/.git_config_backup/ --work-tree=$HOME'
```
4. Install the configuration
```sh
config checkout
```
- Note: if there is an error while "checking out" just remove the file(s) that conflic.

